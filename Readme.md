[![Build Status](https://api.honeybeedev.com/latestVersion/com.oop/data/icon?width=400px)](https://api.honeybeedev.com/latestVersion/com.oop/data/downloadUrl)

## Data Module By OOP-778
Simple, Effective to use data module that supports SQL databases (MySQl, SQLite), Flat Files as JSON

# Full Version with dependencies
## Maven
```xml
<repository>
    <id>code-mc</id>
    <url>https://repo.codemc.org/repository/maven-releases/</url>
</repository>

<dependency>
    <groupId>com.oop</groupId>
    <artifactId>data</artifactId>
    <version>latest build-full</version>
</dependency>
```

## Gradle
```groovy
repositories {
    maven { url 'https://repo.codemc.org/repository/maven-releases/' }
}

dependencies {
    compile "com.oop:data:latest build-full"
}
```

# Version without dependencies
For spigot development
## Maven
```xml
<repository>
    <id>code-mc</id>
    <url>https://repo.codemc.org/repository/maven-releases/</url>
</repository>

<dependency>
    <groupId>com.oop</groupId>
    <artifactId>data</artifactId>
    <version>latest build</version>
</dependency>
```

## Gradle
```groovy
repositories {
    maven { url 'https://repo.codemc.org/repository/maven-releases/' }
}

dependencies {
    compile "com.oop:data:latest build"
}
```